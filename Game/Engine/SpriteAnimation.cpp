//----------------------------------------------------------------------------------------
//	Copyright 2020 Matt Whiting, All Rights Reserved.
//  For educational purposes only.
//  Please do not distribute or republish in electronic or print form without permission.
//  Thanks - whitingm@usc.edu
//----------------------------------------------------------------------------------------

#include "SpriteAnimation.h"
#include <assert.h>

SpriteAnimation::SpriteAnimation()
{
    m_frame = 0.0f;
    m_frameRate = 20.0f;
    m_stopAtEnd = false;
    m_isDone = false;
    m_width = 0;
    m_height = 0;
}

SpriteAnimation::SpriteAnimation(const std::vector<SDL_Texture*>& images)
{
    m_images = images;
    m_frame = 0.0f;
    m_frameRate = 5.0f;
    m_width = 0;
    m_height = 0;
    if (false == images.empty())
        SDL_QueryTexture(images[0], nullptr, nullptr, &m_width, &m_height);
}

void SpriteAnimation::AddImage(SDL_Texture* image)
{
    m_images.emplace_back(image);
}

void SpriteAnimation::SetFrameRate(float rate)
{
    assert(rate >= 0.0f);
    m_frameRate = rate;
}

void SpriteAnimation::Update(float dt)
{
    int numImage = m_images.size();
    if (numImage > 0)
    {
        m_frame += m_frameRate * dt;
        if (m_stopAtEnd)
        {
            if (m_frame >= numImage - 1)
            {
                m_frame = (float)numImage - 1;
                m_isDone = true;
            }
        }
        else
        {
            while (m_frame >= numImage)
            {
                m_frame -= numImage;
                m_isDone = true;
            }
        }
    }
}

void SpriteAnimation::Draw(SDL_Renderer* renderer, Vector2 pos, float rot, Vector2 offset, float scale)
{
    SDL_Texture* tex = GetCurrentImage();
    if (tex)
    {
        SDL_QueryTexture(tex, nullptr, nullptr, &m_width, &m_height);

        SDL_Rect r;
        r.w = static_cast<int>(m_width * scale);
        r.h = static_cast<int>(m_height * scale);
        r.x = static_cast<int>(pos.x - offset.x);
        r.y = static_cast<int>(pos.y - offset.y);

        // Draw (have to convert angle from radians to degrees, and clockwise to counter)
        SDL_RenderCopyEx(renderer,
            tex,
            nullptr,
            &r,
            -Math::ToDegrees(rot),
            nullptr,
            SDL_FLIP_NONE);
    }
}

SDL_Texture* SpriteAnimation::GetCurrentImage()
{
    return m_images[(int)m_frame];
}

float SpriteAnimation::Duration() const
{
    return m_images.size() / m_frameRate;
}

bool SpriteAnimation::IsDone() const
{
    int numImage = m_images.size();
    if (numImage > 0)
        return m_isDone;
    return true;
}

float SpriteAnimation::Ratio() const
{
    return m_frame / m_images.size();
}

void SpriteAnimation::Reset()
{
    m_frame = 0;
    m_isDone = false;
}

