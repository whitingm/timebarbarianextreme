//----------------------------------------------------------------------------------------
//	Copyright 2020 Matt Whiting, All Rights Reserved.
//  For educational purposes only.
//  Please do not distribute or republish in electronic or print form without permission.
//  Thanks - whitingm@usc.edu
//----------------------------------------------------------------------------------------

#include "GameObject.h"
#include "SpriteAnimation.h"

GameObject::GameObject(std::vector<SDL_Texture*> images, Vector2 pos)
{
    m_sprite = new SpriteAnimation(images);
    m_pos = pos;
    if (nullptr != m_sprite)
        m_origin = Vector2((float)m_sprite->Width() / 2.0f, (float)m_sprite->Height() / 2.0f);
    else
        m_origin = Vector2::Zero;
    m_rot = 0.0f;
    m_scale = 1.0f;
}

GameObject::~GameObject()
{
    delete m_sprite;
}

/*virtual*/ void GameObject::Update(float dt)
{
    m_sprite->Update(dt);
}

/*virtual*/ void GameObject::Draw(SDL_Renderer* renderer)
{
    DrawSprite(renderer, m_sprite);
#if DEBUG_COLLBOX
    Rectangle aabb = GetAABB();
    spriteBatch.Draw(s_debugBox, aabb, Color.White);
#endif
}

void GameObject::DrawSprite(SDL_Renderer* renderer, SpriteAnimation* sprite)
{
    sprite->Draw(renderer, GetScreenPos(), m_rot, GetImageOffset(), m_scale);
}

Vector2 GameObject::GetScreenPos() const
{
#if 1
    return m_pos;
#else
    float halfWidth = 0.5f * Game::GetScreenRect().Width;
    Vector2 pos = m_pos;// -Camera.GetPos();
    pos.x -= halfWidth;
    while (pos.x < -Game::s_worldWidth)
        pos.x += 2.0f * Game::s_worldWidth;
    while (pos.x > Game::s_worldWidth)
        pos.x -= 2.0f * Game::s_worldWidth;
    pos.x += halfWidth;
    return pos;
#endif
}

Vector2 GameObject::GetImageOffset() const
{
    Vector2 offset = m_origin;
    //if (m_flip == SpriteEffects.FlipHorizontally)
    //{
    //    if (null != m_sprite)
    //    {
    //        offset.x = m_sprite.Width - offset.x;
    //    }
    //}
    return offset;
}



