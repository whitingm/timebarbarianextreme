//----------------------------------------------------------------------------------------
//	Copyright 2020 Matt Whiting, All Rights Reserved.
//  For educational purposes only.
//  Please do not distribute or republish in electronic or print form without permission.
//  Thanks - whitingm@usc.edu
//----------------------------------------------------------------------------------------

#pragma once
#include "SDL/include/SDL/SDL.h"
#include "GameMath.h"
#include <vector>

class SpriteAnimation
{
public:
    SpriteAnimation();
    SpriteAnimation(const std::vector<SDL_Texture*>& images);
    void AddImage(SDL_Texture* image);
    void SetFrameRate(float rate);
    void StopAtEnd() { m_stopAtEnd = true; }
    void Update(float dt);
    void Draw(SDL_Renderer* renderer, Vector2 pos, float rot, Vector2 offset, float scale);

    int Width() const { return m_width; }
    int Height() const { return m_height; }
    float Duration() const;
    bool IsDone() const;
    float Ratio() const;

    void Reset();

private:
    std::vector<SDL_Texture*> m_images;
    float m_frame;
    float m_frameRate;
    bool m_stopAtEnd;
    bool m_isDone;
    int m_width;
    int m_height;

    SDL_Texture* GetCurrentImage();
};