//----------------------------------------------------------------------------------------
//	Copyright 2020 Matt Whiting, All Rights Reserved.
//  For educational purposes only.
//  Please do not distribute or republish in electronic or print form without permission.
//  Thanks - whitingm@usc.edu
//----------------------------------------------------------------------------------------
#pragma once
#include "GameMath.h"
#include "SDL/include/SDL/SDL.h"
#include <vector>

class SpriteAnimation;

class GameObject
{
public:
    GameObject(std::vector<SDL_Texture*> images, Vector2 pos);
    ~GameObject();
    virtual void Update(float dt);
    virtual void Draw(SDL_Renderer* renderer);
    Vector2 GetPos() const { return m_pos; }
    Vector2 GetScreenPos() const;
    Vector2 GetImageOffset() const;

protected:
    SpriteAnimation* m_sprite;
    Vector2 m_pos;
    Vector2 m_origin;
    float m_rot;
    float m_scale;

    void DrawSprite(SDL_Renderer* renderer, SpriteAnimation* sprite);
};

