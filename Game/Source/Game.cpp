//----------------------------------------------------------------------------------------
//	Copyright 2011-2020 Matt Whiting, All Rights Reserved.
//  For educational purposes only.
//  Please do not distribute or republish in electronic or print form without permission.
//  Thanks - whitingm@usc.edu
//----------------------------------------------------------------------------------------

#include "Game.h"
#include "GameObject.h"
#include "SDL/include/SDL/SDL_image.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 720;

GameObject* pObj;

//----------------------------------------------------------------------------------------
// PUBLIC FUNCTIONS
//----------------------------------------------------------------------------------------
Game::Game()
	:mWindow(nullptr)
	, mRenderer(nullptr)
	, mIsRunning(true)
{
	m_state = State::GAME_ON;
}

bool Game::Initialize()
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0)
	{
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		return false;
	}

	mWindow = SDL_CreateWindow("Time Barbarian Extreme!", 100, 100, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
	if (!mWindow)
	{
		SDL_Log("Failed to create window: %s", SDL_GetError());
		return false;
	}

	mRenderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (!mRenderer)
	{
		SDL_Log("Failed to create renderer: %s", SDL_GetError());
		return false;
	}

	if (IMG_Init(IMG_INIT_PNG) == 0)
	{
		SDL_Log("Unable to initialize SDL_image: %s", SDL_GetError());
		return false;
	}

	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);

	LoadContent();

	mTicksCount = SDL_GetTicks();

	return true;
}

void Game::RunLoop()
{
	while (mIsRunning)
	{
		// Compute delta time
		// Wait until 16ms has elapsed since last frame
		while (!SDL_TICKS_PASSED(SDL_GetTicks(), mTicksCount + 16))
			;

		float deltaTime = (SDL_GetTicks() - mTicksCount) / 1000.0f;
		if (deltaTime > 0.033f)
			deltaTime = 0.033f;
		mTicksCount = SDL_GetTicks();

		ProcessInput();
		Update(deltaTime);
		Draw();
	}
}

void Game::Shutdown()
{
	UnloadContent();
	Mix_CloseAudio();
	IMG_Quit();
	SDL_DestroyRenderer(mRenderer);
	SDL_DestroyWindow(mWindow);
	SDL_Quit();
}

/*static*/ void Game::DrawTextCenterAtY(const std::string& text, float y, float scale)
{
	//mrwTODO
}

/*static*/ void Game::DrawTextCenter(const std::string& text, Vector2 offset, float scale)
{
	//mrwTODO
}

/*static*/ void Game::DrawTextCenter(const std::string& text, Vector2 offset)
{
	//mrwTODO
}

/*static*/ void Game::DrawTextCenter(const std::string& text)
{
	//mrwTODO
}

SDL_Texture* Game::GetTexture(const std::string& fileName)
{
	SDL_Texture* tex = nullptr;
	auto iter = mTextures.find(fileName);
	if (iter != mTextures.end())
	{
		tex = iter->second;
	}
	else
	{
		// Load from file
		SDL_Surface* surf = IMG_Load(fileName.c_str());
		if (!surf)
		{
			SDL_Log("Failed to load texture file %s", fileName.c_str());
			return nullptr;
		}

		// Create texture from surface
		tex = SDL_CreateTextureFromSurface(mRenderer, surf);
		SDL_FreeSurface(surf);
		if (!tex)
		{
			SDL_Log("Failed to convert surface to texture for %s", fileName.c_str());
			return nullptr;
		}

		mTextures.emplace(fileName, tex);
	}
	return tex;
}


//----------------------------------------------------------------------------------------
// PRIVATE FUNCTIONS
//----------------------------------------------------------------------------------------
void Game::LoadContent()
{
	std::vector<SDL_Texture*> images;

	char name[1024];
	for (int i = 1; i <= 9; i++)
	{
		snprintf(name, sizeof(name), "Assets/Dudes/Player/Joust_Char_0100%02d.png", i);
		images.emplace_back(GetTexture(name));
	}
	pObj = new GameObject(images, Vector2(100.0f, 100.0f));
}

void Game::UnloadContent()
{
	delete pObj;
}

void Game::ProcessInput()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			mIsRunning = false;
			break;
		}
	}

	const Uint8* state = SDL_GetKeyboardState(NULL);
	if (state[SDL_SCANCODE_ESCAPE])
	{
		mIsRunning = false;
	}
}

void Game::Update(float dt)
{
	pObj->Update(dt);
}

void Game::Draw()
{
	SDL_SetRenderDrawColor(mRenderer, 0, 0, 0, 255);
	SDL_RenderClear(mRenderer);

	switch (m_state)
	{
	case State::TITLE_SCREEN:
		break;

	case State::PRESS_START:
		break;

	case State::FRONT_END_MENU:
		break;

	case State::NEW_GAME_WARNING:
		break;

	case State::LOADGLOBAL:
	case State::LOADING:
	case State::START_GAME:
		break;

	case State::GAME_ON:
		DrawGame();
		break;

	case State::PAUSE:
		break;

	case State::GAME_OVER:
	case State::RELOAD_SAVE:
		DrawGame();
		DrawTextCenter("Game Over");
		break;

	case State::GAME_OVER_MENU:
		break;

	case State::STAGE_CLEAR:
		DrawGame();
		DrawTextCenter("Stage Clear");
		break;

	case State::STAGE_WARP:
		break;

	case State::UPGRADE:
		break;

	case State::OPTIONS_MAIN:
		break;

	case State::CREDITS:
		break;

	case State::LEADERBOARD_MENU:
		break;

	case State::LEADERBOARD_DEAD:
		break;
	}

	SDL_RenderPresent(mRenderer);
}

void Game::DrawGame()
{
	pObj->Draw(mRenderer);
}


