//----------------------------------------------------------------------------------------
//	Copyright 2011-2020 Matt Whiting, All Rights Reserved.
//  For educational purposes only.
//  Please do not distribute or republish in electronic or print form without permission.
//  Thanks - whitingm@usc.edu
//----------------------------------------------------------------------------------------

#pragma once
#include "SDL/include/SDL/SDL.h"
#include "SDL/include/SDL/SDL_mixer.h"
#include <unordered_map>
#include <string>
#include <vector>
#include "GameMath.h"

class Game
{
public:
	enum class State
	{
		LOAD_SAVE,
		TITLE_SCREEN,
		PRESS_START,
		RELOAD_SAVE,
		FRONT_END_MENU,
		NEW_GAME_WARNING,
		LOADGLOBAL,
		LOADING,
		START_GAME,
		GAME_ON,
		PAUSE,          // includes the options menu from the pause screen
		GAME_OVER,
		GAME_OVER_MENU,
		STAGE_CLEAR,
		STAGE_WARP,
		UPGRADE,
		OPTIONS_MAIN,   // on the options menu from the main screen
		CREDITS,
		LEADERBOARD_MENU,
		LEADERBOARD_DEAD,
#if USC_GAMES_EXPO
		ENTER_NAME,
#endif
	};

	Game();
	bool Initialize();
	void RunLoop();
	void Shutdown();

	static State GetState();
	static void SetState(State newState);

	static void ContinueGame();

	static void DrawTextCenterAtY(const std::string& text, float y, float scale);
	static void DrawTextCenter(const std::string& text, Vector2 offset, float scale);
	static void DrawTextCenter(const std::string& text, Vector2 offset);
	static void DrawTextCenter(const std::string& text);

	static void SetBackgroundDepth(float depth);

//	static Rectangle GetScreenRect();
	static float GetLavaHeight();
	static Vector2 WrapPos(Vector2 dir);
	static void BulletTime(float factor, float timer);
	static float GetActualDT();

	SDL_Texture* GetTexture(const std::string& fileName);


private:
	void LoadContent();
	void UnloadContent();
	void ProcessInput();
	void Update(float dt);
	void Draw();

	void StartNewGame();

	void DrawGame();
	void DrawTitleScene();
	void DrawTitle();
	void DrawControls();

	void UpdateBulletTime(float dt);
	void UpdateGame(float dt);
	void UpdateTitle(float dt);

	void SetSoundVolume(float volume);
	void SetMusicVolume(float volume);
	//void SetMusic(Song song);
	void UpdateMusic(float dt);

	void NextHint();
	void UpdateSavedSettings();

	SDL_Window* mWindow;
	SDL_Renderer* mRenderer;
	Uint32 mTicksCount = 0;
	bool mIsRunning;

	State m_state;

	std::unordered_map<std::string, SDL_Texture*> mTextures;
};
