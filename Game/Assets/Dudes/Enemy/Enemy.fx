sampler SceneTexture : register(s0);
float invert = 0.0;

float4 InvertPS(float2 uv : TEXCOORD0, float4 vertColor : COLOR0) : COLOR0
{
	float4 color = tex2D(SceneTexture, uv.xy);
	float brightness = dot(color.xyz, color.xyz);
	float4 invertColor = float4(0.4f * brightness, 0.6f * brightness, 1.1f * brightness, color.w);
	color = lerp(color, invertColor, invert);
	color.xyz = lerp(color.xyz, vertColor.xyz, 1.0f - vertColor.w);
	return color;
}

technique Invert
{
    pass
    {
        //set sampler states
        MagFilter[0] = LINEAR;
        MinFilter[0] = LINEAR;
        MipFilter[0] = LINEAR;
        AddressU[0] = CLAMP;
        AddressV[0] = CLAMP;
        
        // set render states
        AlphaBlendEnable = TRUE;

        PixelShader = compile ps_2_0 InvertPS();
    }
}
